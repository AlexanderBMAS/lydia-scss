# Lydia - SCSS
BMAS SCSS Base Framework Framework

Lydia SCSS is a design agnostic SCSS framework that is the basis for BMAS web development projects. Its aim is to not to enforce a particular style similar to Bootstrap, but rather provide a set of front end tools for quickly building layouts and common ui elements. The base styles applied are un-opiniated and easily extendible to suit the visual styles of the client site.

## Features
* Browser Reset
* px to rem conversion
* Intuative Media Query's
* Print conversion
* Agnostic component styles
* Global configuration
* Easily Exctendable
* Common vendor intergrations

##Instalation
Lydia SCSS is primarily a submodule of Lydia-WP but can be complied as a standalone stylesheet.

## Vendors
* Eric Meyers CSS Reset : https://meyerweb.com/eric/tools/css/reset/
* px to rem converison : https://github.com/pierreburel/sass-rem
* mq Media Queries : https://github.com/sass-mq/sass-mq

## Dependencies
* Autoprefixer
* SCSS compiler